//CRUD Operations
	//CRUD operation is the heart of any backend application
	//mastering the CRUD operations is essential for any developer
	// This helps in building character and increasing exposure to logical statements that will help us manipulate
	//mastering the CRUD operations of any languages makes us a valuable developer and makes the work easier for us to deal with huge amounts of information


	//[Section] Create (Inserting document);
		//since MongoDB deals with object as it's structure for documents, we can easily create them by providing objects into our methods.
		//mongoDB shell also uses javascript for it's syntax which makes it convenient for us to understand it's code.

		//Insert One document
			/*
				Syntax:
					db.collectionName.insertOne({object/document})
			*/

		
		db.users.insertOne({
			firstName: "Jane",
			lastName: "Doe",
			age: 21,
			contact:{
				phone: "1234567890",
				email: "janedoe@gmail.com"
			},
			courses: ["CSS", "Javascript", "Python"],
			department: "none"
		});

		//Insert Many
		/*
			db.users.insertMany([{objectA}, {objectB}])
		*/


		db.users.insertMany([{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact:{
				phone: "87654321",
				email: "stephenhawking@gmail.com"
			},
			courses: ["Python", "React", "PHP"],
			department: "none"
		}, {
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact:{
				phone: "87654321",
				email: "neilarmstrong@gmail.com"
			},
			courses: ["React", "Laravel", "Sass"],
			department: "none"
		}]);

		db.users.insertOne({
			firstName: "Jane",
			lastName: "Doe",
			age: 21,
			contact:{
				phone: "1234567890",
				email: "janedoe@gmail.com"
			},
			courses: ["CSS", "Javascript", "Python"],
		});

		db.users.insertOne({			
			lastName: "Doe",
			age: 21,
			contact:{
				phone: "1234567890",
				email: "janedoe@gmail.com"
			},
			courses: ["CSS", "Javascript", "Python"],
			firstName: "Jane",
		});

			db.users.insertOne({
			firstName: "Jane",
			lastName: "Doe",
			age: 21,
			contact:{
				phone: "1234567890",
				email: "janedoe@gmail.com"
			},
			courses: ["CSS", "Javascript", "Python"],
			gender: "Female"
		});

			db.users.insertOne({
			firstName: "Jane",
			lastName: "Doe",
			age: 21,
			contact:{
				phone: "1234567890",
				phone: "123456789011",
				email: "janedoe@gmail.com"
			},
			courses: ["CSS", "Javascript", "Python"],
			gender: "Female"
		});


		//[Section] Find (read)

			/*
				Syntax:
					//it will show us all the documents in our collection
					db.collectionName.find();
					
					//it will show us all the documents that has the given fieldset | case sensitive
					db.collectionName.find({field: value})
			*/


		db.users.find();

		db.users.find({age : 76});

		db.users.find({firstName : "jane"});

		db.users.find({firstName : "Jane"});


			//finding documents using multiple fieldsets
		/*
			Syntax:
				db.collectionName.find({fieldA:valueA, fieldB: valueB})
		*/


		db.users.find({lastName: "Armstrong", age: 83});

		//[Section] Updating Documents

		//add document
		db.users.insertOne({
			firstName: "Test",
			lastName: "Test",
			age: 0,
			contact:{
				phone: "000000000",
				email: "test@gmail.com"
			},
			course: [],
			department: "none"
		});

			//updateOne
				/*
					Syntax:
					db.collectionName.updateOne({criteria}, {$set: {field:value}});
				*/
				
		db.users.updateOne({
			firstName: "Jane"
		}, {
			$set:{
				lastName: "Hawking"
			}
		})
			
			//updating multiple documents
			/*
				db.collectionName.updateMany({criteria},{
				$set: {
				field:value
				}
				}})
			*/

		db.users.updateMany({firstName: "Jane"}, {
			$set:{
				lastName: "Wick",
				age: 25
			}
		})

		db.users.updateMany({department:"none"},{
			$set:{
				department: "HR"
		}
	})

		//replacing the old document
		/*
			db.users.replaceOne({criteria}, {document/objectToReplace})
		*/



		db.users.replaceOne({firstName: "Test"}, {
			firstName: "Chris",
			lastName: "Mortel"
		})


		//[Section] Deleting documents


			//[Section] Deleting single document
		
			/*
				db.collectionName.deleteOne({criteria});
			*/


		db.users.deleteOne({firstName: "Jane"});
			//Delete many
			/*
			Syntax/:
			db.collectionName.deleteMany({criteria});
			*/

		db.users.deleteMany({firstName: "Jane"});


		//reminder!!
			// X db.collectionName.deleteMany()
			//Remind: dont forget to add criteria!!

		//[Section] Advanced Queries
			//embedded - object
			//nested field - array

		//query on an embedded document
		db.users.find({"contact.phone": "87654321"});

		//query on array with exact element
		db.users.find({courses: ["React", "Laravel", "Sass"]})

		//querying an array without regards to order and elements
		db.users.find({courses: {$all: ["React"]}});


		db.collectionName.deleteMany()